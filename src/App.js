import React, { useState } from 'react';
import { BsArrowCounterclockwise } from "react-icons/bs";
import ButtonThumb from './components/ButtonThumb';
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  
  const colorB = (num) => {
    return num === 0 ? 'XC' : num < 0 ? 'cRed': 'cGreen'
  }

  return (
    <div className="App">
      <header className="App-header">
      
      <div>
        <ButtonThumb value={count + 1} type={"up"} setCount={setCount}/>
        <ButtonThumb value={count - 1} type={"down"} setCount={setCount}/>
      </div>

      <p >
        El numero de reacciones actual es:  
        <ul>
          <span className={colorB(count)}>{count}</span>
        </ul>
      </p>

      <button onClick={() => setCount(0)}>
        <BsArrowCounterclockwise style={{fontSize: '50px'}}/>
      </button>

      </header>
    </div>
  );
}

export default App;
