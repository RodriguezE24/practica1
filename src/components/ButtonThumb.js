import React from 'react';
import {BsFillHandThumbsUpFill, BsFillHandThumbsDownFill} from "react-icons/bs";

const ButtonThumb = ({value, type, setCount}) => {
    return <button onClick={()=>setCount(value)}>
        {
            type === "up" ?
            <BsFillHandThumbsUpFill style={{color: 'blue', fontSize: '50px'}}/>:
            <BsFillHandThumbsDownFill style={{color: 'red', fontSize: '50px'}}/>
        }
    </button>

}

export default ButtonThumb